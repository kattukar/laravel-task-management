<?php

namespace App\Http\Middleware;

use Closure;

class ResponseAllowHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (method_exists($response, 'header')) {
            cors($response);

            if ($request->method() === 'OPTIONS') {
                $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
            }
        }

        return $response;
    }
}
