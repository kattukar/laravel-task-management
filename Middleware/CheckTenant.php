<?php

namespace App\Http\Middleware;

use Closure;

class CheckTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $org = extract_tenantID_from_URL();
        if (! is_null($org) && auth_user()) {

            if (! support_user()) {
                verify_organization($org);
                $userOrganizations = auth_user()->organizations()->select('organizations.id')->pluck('id')->all();
                if (empty($userOrganizations)) {
                    abort(401, 'You do not belong to any organization');
                }
                else if(! in_array($org, $userOrganizations)) {
                    abort(401, 'You do not belong to this organization');
                }
            }

            set_user_last_accessed($org, auth_user()->id);
        }

        return $next($request);
    }
}
