<?php

namespace App\Http\Middleware;

use Closure;

class SetTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantService = app('Tallyfy\API\App\Services\TenantService');
        $tenantService->setTenant($request->input('ctrl_defined_org_id'));

        $request->replace($request->except('ctrl_defined_org_id'));

        return $next($request);
    }
}
