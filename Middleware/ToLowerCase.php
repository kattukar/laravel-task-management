<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;
use Str;

class ToLowerCase extends TransformsRequest
{
    /**
     * The attributes that should be converted to lower case.
     *
     * @var array
     */
    protected $fields = [
        'email'
    ];

    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function transform($key, $value)
    {
        if (in_array($key, $this->fields)) {
            return Str::lower(trim($value));
        }

        return $value;
    }
}
