<?php
namespace App\Http\Controllers\Checklists;

use App\Http\Controllers\BaseControllerNew;
use Tallyfy\API\App\Services\StepService;
use Tallyfy\API\V1\Transformers\CaptureTransformer;

class StepCapturesController extends BaseControllerNew
{
    protected $service;
    protected $transformer;

    public function __construct(StepService $service, CaptureTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;

        parent::__construct();
    }

    /**
     * @SWG\Post(
     *     path="/organizations/{org}/steps/{step_id}/captures",
     *     description="Add a Form Field to a Step in a Blueprint. Dropdown, multiselect, and radio forms require the `options` property to be filled out",
     *     summary="Add a Form Field",
     *     operationId="addCapture",
     *     produces={"application/json"},
     *     tags={"Step"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization ID",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="step_id",
     *         in="path",
     *         description="Step ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         description="",
     *         @SWG\Schema(ref="#/definitions/CaptureInput")
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successful response",
     *         schema = @SWG\Definition (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaptureResponse"
     *              )
     *         )
     *     )
     * )
     */
    public function store($orgID, $stepID)
    {
        $stepID = aliveID($stepID);

        $request = \App::make('Tallyfy\API\Http\Requests\Captures\CreateCaptureRequest', ['step_id' => $stepID]);

        $capture = $this->service->addCapture($stepID, $request->all());

        return $this->respondWithItem($capture, 201);
    }

    /**
     * @SWG\Put(
     *     path="/organizations/{org}/steps/{step_id}/captures/{capture_id}",
     *     description="Update a Form Field",
     *     summary="Update a Form Field",
     *     operationId="updateCapture",
     *     produces={"application/json"},
     *     tags={"Step"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step_id",
     *         in="path",
     *         description="Step ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="capture_id",
     *         in="path",
     *         description="Form Field ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         description="",
     *         @SWG\Schema(ref="#/definitions/CaptureInput")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful response",
     *         schema = @SWG\Definition (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaptureResponse"
     *              )
     *         )
     *     )
     * )
     */
    public function update($orgID, $stepID, $captureID)
    {
        $stepID = aliveID($stepID);
        $captureID = aliveID($captureID);

        $request = \App::make('Tallyfy\API\Http\Requests\Captures\CreateCaptureRequest', ['step_id' => $stepID]);

        $capture = $this->service->editCapture($stepID, $captureID, $request->all());

        return $this->respondWithItem($capture, 201);
    }

    /**
     * @SWG\Delete(
     *     path="/organizations/{org}/steps/{step_id}/captures/{capture_id}",
     *     description="Delete a Form Field",
     *     summary="Delete a Form Field",
     *     operationId="deleteCapture",
     *     produces={"application/json"},
     *     tags={"Step"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step_id",
     *         in="path",
     *         description="Step ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="capture_id",
     *         in="path",
     *         description="Form Field ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful response"
     *     )
     * )
     */
    public function destroy($orgID, $stepID, $captureID)
    {
        $stepID = aliveID($stepID);
        $captureID = aliveID($captureID);

        $this->service->deleteCapture($stepID, $captureID);

        return $this->respondWithEmpty(204);
    }
}