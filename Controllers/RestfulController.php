<?php namespace App\Http\Controllers;

use App;
use Tallyfy\API\App\Exceptions\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Session;
use Tallyfy\API\V1\Repositories\Eloquent\BaseRepository;
use Input;

/**
 * Class RestfulController
 * @package App\Http\Controllers
 */
class RestfulController extends BaseControllerOld
{
    /**
     * @property Transformer transformer
     */
    protected $transformer;

    /**
     * @var
     */
    protected $defaultSortBy;

    /**
     * @var
     */
    protected $input;


    /**
     * @property BaseRepository repository
     */
    protected $repository;

    /**
     * @var
     */
    protected $validator;


    protected $permission_prefix;


    function __construct()
    {
        if (!isset($this->request)) {
            $this->request = new Request;
        }
        parent::__construct();
    }

    /**
     * @return IlluminatePaginatorAdapter
     */
    public function index()
    {
        hasAccess($this->permission_prefix . '.view');
        $limit = Input::get('per_page', 10);
        $sort = Input::get('sort', $this->defaultSortBy);
        $query = Input::get('q');

        $collection = $this->repository
            ->search($query)
            ->sortBy($sort)
            ->filter()
            ->getPaginated($limit);

        return $this->withPaginator($collection, $this->transformer);
    }

    public function getAll()
    {
        hasAccess($this->permission_prefix . '.view');

        $limit = Input::get('per_page', 10);
        $sort = Input::get('sort', $this->defaultSortBy);
        $query = Input::get('q');

        $collection = $this->repository
                            ->search($query)
                            ->sortBy($sort)
                            ->filter()
                            ->getAll($limit);

        return $this->withCollection($collection, $this->transformer);
    }


    /**
     * @param $key
     * @return mixed
     */
    public function show($key)
    {
        hasAccess($this->permission_prefix . '.view');
        $record = $this->repository->getByKey($key, true, true);
        return $this->withItem($record, $this->transformer);
    }

    /**
     *
     */
    public function store()
    {
        hasAccess($this->permission_prefix . '.create');
        $this->prepareInput($this->repository->model->default);
        if ($this->validateInput()) {
            $input = $this->input ? $this->input : $this->request->all();
            if ($record = $this->repository->create($input)) {
                return $this->setStatusCode(201)->withItem($record, $this->transformer);
            }
        }
        throw new StoreResourceFailedException('Could not create new record.', $this->validator->errors());

    }

    /**
     * @param $key
     */
    public function update($key)
    {
        if (isset($this->permissions) && isset($this->permissions['update'])) {
            hasAccess($this->permissions['update']);
        } else {
            hasAccess($this->permission_prefix . '.update');
        }

        $current = $this->repository->validateUpdate($key);
        $this->prepareInput(array_only($current->toArray(), $this->repository->model->fields));

        if ($this->validateInput($key)) {
            if ($this->repository->update($key, $this->input)) {
                return $this->setStatusCode(200)->withItem($this->repository->getByKey($key), $this->transformer);
            }
        }

        throw new UpdateResourceFailedException(\Session::get('error'), $this->validator->errors());
    }

    /**
     * @param $key
     * @return mixed
     */
    public function destroy($key)
    {
        hasAccess($this->permission_prefix . '.archive');
        $this->repository->validateDestroy($key);
        if ($this->repository->archive($key)) {
            return $this->setStatusCode(200)->withItem($this->repository->getByKey($key, true, true),
                $this->transformer);
        }
        throw new DeleteResourceFailedException(Session::get('error'));
    }

    /**
     * @param $key
     * @return mixed
     */
    public function delete($key)
    {
        $item = $this->repository->getByKey($key, false, true);
        if ($this->repository->delete($key)) {
            return $this->setStatusCode(200)->withItem($item, $this->transformer);
        }
        throw new DeleteResourceFailedException(Session::get('error'));
    }

    /**
     * @param $key
     * @return mixed
     * @throws ResourceFailedException
     */
    public function restore($key)
    {
        hasAccess($this->permission_prefix . '.restore');
        if ($this->repository->restore($key)) {
            return $this->setStatusCode(200)->withItem($this->repository->getByKey($key), $this->transformer);
        }
        throw new ResourceException(Session::get('error'));
    }

}
