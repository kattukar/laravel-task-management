<?php

namespace App\Http\Controllers\Checklists;

use App\Http\Controllers\TenantsController;
use Tallyfy\API\V1\Repositories\Eloquent\ChecklistsRepository;
use Tallyfy\API\V1\Transformers\ChecklistTransformer;
use Tallyfy\API\Exceptions\AccessDeniedException;

class ChecklistsController extends TenantsController
{

    protected $withMap = [
        'default' => [
            'kickOffFields',
            'statistics'
        ],
        'tags' => [
            'tags'
        ]
    ];

    function __construct(ChecklistTransformer $transformer, ChecklistsRepository $repo)
    {
        $this->repository = $repo;
        $this->transformer = $transformer;
        $this->permission_prefix = 'checklists';
        $this->defaultSortBy = '-checklists.last_updated';
        parent::__construct();
        ini_set('max_execution_time', 60);
    }

    function update($tenant)
    {
        set_tenant($tenant);
        $checklistId = \Request::segment($this->key_segment) ?: $this->id;
        $this->prepareInput($this->repository->model->default);
        if ($this->validateInput($checklistId)) {
            $checklist = $this->repository->getByKey($checklistId);
            if ((\API::user()->id == $checklist->user_id) || \API::user()->hasAccess($this->permission_prefix . '.update')) {
                return $this->repository->update($checklistId, $this->input);
            } else {
                throw new AccessDeniedException('You do not have permissions to perform this task', 403);
            }
        }
    }

    /**
     * @SWG\Put(
     *     path="/organizations/{org}/checklists/{id}/restore",
     *     description="Restore an archived Blueprint",
     *     summary="Unarchive a Blueprint",
     *     operationId="restoreArchivedChecklist",
     *     produces={"application/json"},
     *     tags={"Template"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization Id",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Blueprint Id",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success response",
     *         schema = @SWG\Definition (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Checklist"
     *              )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Blueprint not found",
     *         @SWG\Schema(
     *             ref="#/definitions/error"
     *         )
     *     ),
     *    @SWG\Response(
     *         response="403",
     *         description="Do not have permission",
     *         @SWG\Schema(
     *             ref="#/definitions/error"
     *         )
     *     )
     * )
     *
     */

    public function delete($org)
    {
        $data = [];
        if (\API::user()->is_support) {
            $data = parent::delete($org);
        }
        return \Response::json(['data' => $data]);
    }
}
