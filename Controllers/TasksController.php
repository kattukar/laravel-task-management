<?php namespace App\Http\Controllers\Checklists;

use Tallyfy\API\Domain\Observers\Checklists\Run\Traits\ManageTasks;
use App\Http\Controllers\TenantsController;
use Tallyfy\API\V1\Models\Run;
use Tallyfy\API\V1\Models\Task;
use Tallyfy\API\V1\Repositories\Eloquent\TasksRepository;
use Tallyfy\API\V1\Transformers\TaskTransformer;
use Tallyfy\API\App\Traits\ResendTasks;

class TasksController extends TenantsController
{
    use ManageTasks, ResendTasks;

    private $sendOwnerEmail = true;
    private $notifyOwnerChanged = true;
    private $sendGuestsEmail = true;

    protected $withMap = [
        'default' => [
            'standaloneTask',
            'captureValues',
            'owners',
            'guests'
        ],
        'step' => [
            'step.rules',
            'step.captures'
        ],
        'run' => [
            'run.tasks.threads',
        ],
        'threads' => [
            'threads'
        ]
    ];
    
    public function __construct(TaskTransformer $transformer, TasksRepository $repo)
    {
        $this->repository = $repo;
        $runParamPosition = array_search('runs', \Request::segments());
        $this->repository->run_id = ($runParamPosition > -1) ? \Request::segment($runParamPosition + 2) : null;
        $this->transformer = $transformer;
        $this->key_segment = array_search('tasks', \Request::segments()) + 2;
        $this->permission_prefix = 'tasks';
        $this->defaultSortBy = 'created_at';
        parent::__construct();
    }

    /**
     * @SWG\GET(
     *     path="/organizations/{org}/tasks",
     *     description="Get all Tasks of all members in an organization. Tasks in different Processes that originated from the same Blueprint will have different Task IDs, but will have the same Task aliases from the original Blueprint.",
     *     summary="Get all Tasks in an organization",
     *     operationId="getAllTasks",
     *     tags={"Task"},
     *     produces={
     *         "application/json"
     *     },
     *     @SWG\Parameter(
     *         name="org",
     *         type="string",
     *         in="path",
     *         description="Organization ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="q",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search Tasks by Process name"
     *     ),
     *     @SWG\Parameter(
     *         name="status",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"complete","hasproblem","overdue", "due_soon","active", "incomplete", "inprogress"},
     *         description="You can search all Tasks by status"
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"deadline","newest","problems","completed_newest","-deadline"},
     *         description="Sort records in order by a given property. Minus sign (-) before sort value sorts in descending order."
     *     ),
     *     @SWG\Parameter(
     *         name="with",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"run","run.checklist","step","threads","comments","assets","run,run.checklist,step,threads,comments,assets"},
     *         description="Retrieve additional data from Tasks. Separate multiple options with a comma"
     *     ),
     *     @SWG\Parameter(
     *         name="tag",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search Tasks by Tag name"
     *     ),
     *    @SWG\Parameter(
     *         name="owners",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search all Tasks belongs to specific members, multiple member Ids separated by comma, Ex: owners=2,1014"
     *     ),
     *     @SWG\Parameter(
     *         name="created",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search all Tasks created in a date range: yyyy-mm-dd:yyyy-mm-dd or in specific date: yyyy-mm-dd"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="Which results page to retrieve. Default is 1"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="How many Tasks per page. Default is 10"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success response",
     *         @SWG\Schema(
     *             ref="#/definitions/TasksListOnlyPagination"
     *         )
     *     )
     * )
     *
     * @SWG\GET(
     *     path="/organizations/{org}/runs/{run_id}/tasks",
     *     description="Get all Tasks from a specific Process",
     *     summary="Get all Tasks in a Process",
     *     operationId="getProcessTasks",
     *     tags={"Task"},
     *     produces={
     *         "application/json"
     *     },
     *     @SWG\Parameter(
     *         name="org",
     *         type="string",
     *         in="path",
     *         required=true,
     *         description="Organization ID"
     *     ),
     *     @SWG\Parameter(
     *         name="run_id",
     *         type="string",
     *         in="path",
     *         required=true,
     *         description="Process ID"
     *     ),
     *     @SWG\Parameter(
     *         name="status",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"complete","hasproblem","overdue", "due_soon","active", "incomplete", "inprogress"},
     *         description="You can search Process Tasks by status"
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"position","deadline","-position","-deadline"},
     *         description="Sort Tasks in ascending order based on position or deadline. A minus sign (-) before sort value will sort records in descending order"
     *     ),
     *     @SWG\Parameter(
     *         name="with",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"run","run.checklist","step","threads","comments","assets","run,run.checklist,step,threads,comments,assets"},
     *         description="Retrieve additional data from Tasks. Separate multiple options with a comma"
     *     ),
     *    @SWG\Parameter(
     *         name="owners",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search Process Tasks assigned to specific members. Separate multiple member Ids by a comma. Ex: owners=10001,10002"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="Which results page to retrieve. Default is 1"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="How many Tasks per page. Default is 10"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success response",
     *         @SWG\Schema(
     *             ref="#/definitions/TasksListOnlyPagination"
     *         )
     *     )
     * )
     */

    public function index($org = null, $run_id = null)
    {
        set_tenant($org);
        if (hasAccess("runs.view")) {
            $this->appendPermissions = auth_user()->appendExtraPermissions(['tasks.view' => 1]);
        }

        if ($run_id) {
            $run = Run::where('id', $run_id)->withTrashed()->firstOrFail();
            if (is_null($run->deleted_at)) {
                $query = $run->allTasks();
            }
            else {
                $query = $run->allTasks()->withTrashed();
            }

            $this->repository->setQuery($query);
        }

        return parent::index($org);
    }


    /**
     * @SWG\GET(
     *     path="/organizations/{org}/users/{user_id}/tasks/completed",
     *     description="Get all completed Tasks that belong to a specific member",
     *     summary="Get a member's completed Tasks",
     *     operationId="getUserCompletedTasks",
     *     tags={"Task"},
     *     produces={
     *         "application/json"
     *     },
     *     @SWG\Parameter(
     *         name="org",
     *         type="string",
     *         in="path",
     *         description="Organization ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="user_id",
     *         type="integer",
     *         in="path",
     *         description="Member Id",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"deadline","newest","problems","completed_newest","-deadline"},
     *         description="Sort records in order by a given property. Minus sign (-) before sort value sorts in descending order."
     *     ),
     *     @SWG\Parameter(
     *         name="q",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search Tasks by Process name"
     *     ),
     *     @SWG\Parameter(
     *         name="with",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"run","run.checklist","step","threads","comments","assets","run,run.checklist,step,threads,comments,assets"},
     *         description="Retrieve additional data from Tasks. Separate multiple options with a comma"
     *     ),
     *     @SWG\Parameter(
     *         name="tag",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search Tasks by Tag name"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="Which results page to retrieve. Default is 1"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="How many Tasks per page. Default is 10"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success response",
     *         @SWG\Schema(
     *             ref="#/definitions/TasksListOnlyPagination"
     *         )
     *     )
     * )
     *
     * @SWG\GET(
     *     path="/organizations/{org}/me/tasks/completed",
     *     description="Get all completed Tasks that belong to the current member",
     *     summary="Get current member's completed Tasks",
     *     operationId="getCurrentUserCompletedTasks",
     *     tags={"Task"},
     *     produces={
     *         "application/json"
     *     },
     *     @SWG\Parameter(
     *         name="org",
     *         type="string",
     *         in="path",
     *         description="Organization ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"deadline","newest","problems","completed_newest","-deadline"},
     *         description="Sort records in order by a given property. Minus sign (-) before sort value sorts in descending order."
     *     ),
     *     @SWG\Parameter(
     *         name="q",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search Tasks by Process name"
     *     ),
     *     @SWG\Parameter(
     *         name="with",
     *         type="string",
     *         in="query",
     *         required=false,
     *         enum={"run","run.checklist","step","threads","comments","assets","run,run.checklist,step,threads,comments,assets"},
     *         description="Retrieve additional data from Tasks. Separate multiple options with a comma"
     *     ),
     *     @SWG\Parameter(
     *         name="tag",
     *         type="string",
     *         in="query",
     *         required=false,
     *         description="Search Tasks by Tag name"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="Which results page to retrieve. Default is 1"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         type="integer",
     *         in="query",
     *         required=false,
     *         description="How many Tasks per page. Default is 10"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success response",
     *         @SWG\Schema(
     *             ref="#/definitions/TasksListOnlyPagination"
     *         )
     *     )
     * )
     */
    public function getUserCompletedTasks($orgId, $userId = null)
    {
        $this->repository->status = TASK_COMPLETED;
        set_tenant($orgId);
        hasAccess($this->permission_prefix . '.view');
        $limit = \Input::get('per_page', 10);
        $sort = \Input::get('sort', '-completed_at');
        $query = \Input::get('q');

        if ($userId === 'me') {
            $userId = \API::user()->id;
        }

        if(!is_null($userId)) {
            $this->repository->setUserId($userId);
        } else {
            $this->repository->setUserId(\API::user()->id);
        }
        $collection = $this->repository
            ->search($query)
            ->sortBy($sort)
            ->filter()
            ->getPaginated($limit);

        return $this->withPaginator($collection);
//        return $this->response
//            ->withPaginator(new IlluminatePaginatorAdapter($collection), $this->transformer);
    }

    /**
     * @SWG\PUT(
     *     path="/organizations/{org}/runs/{run}/tasks/{id}",
     *     description="Update a Task inside a Process",
     *     summary="Update a Task inside a Process",
     *     operationId="updateTask",
     *     tags={"Task"},
     *     produces={
     *         "application/json"
     *     },
     *     @SWG\Parameter(
     *         name="org",
     *         type="string",
     *         in="path",
     *         description="Organization ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="run",
     *         type="string",
     *         in="path",
     *         description="Process ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         required=true,
     *         description="ID of task you want to update"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(
     *              ref="#/definitions/updateTaskInput"
     *         )
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success response",
     *         schema = @SWG\Definition (
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Task")
     *              )
     *         )
     *     )
     * )
     */

    public function update($org, $runID = null, $taskID = null)
    {
        $request = \App::make('Tallyfy\API\Http\Requests\Tasks\UpdateTasksRequest', ['id' => $taskID]);

        //when reassigning tasks, the "multi" option will be attached to the input
        if (\Input::get('multi') === 1) {
            $this->repository->setQuery(Task::getModel());
        }
        $task = $this->repository->update($request->task->id, $request);

        if( $request->has('owners') ) {
            $addedGuests = $this->setGuestOwnersAttribute($task, $request->all());
            $addedOwners = $this->setOwnerAttribute($task, $request->all(), $this->notifyOwnerChanged);
            $currRun = $task->load('run')->run;
            
            if ($this->sendOwnerEmail)
            {
                $this->sendEmailOwners($addedOwners, $currRun);
            }
            
            if ($this->sendGuestsEmail)
            {
                $this->sendEmailGuests($currRun, $task->guests()->whereIn('email', $addedGuests)->get());
            }
        }
        return $this->response->withItem($task->fresh(), $this->transformer);
    }

    /**
     * @SWG\GET(
     *     path="/organizations/{org}/runs/{run_id}/tasks/{id}/email",
     *     description="Send an email reminder to all assignees of a Process Task",
     *     summary="Send email reminder to Process Task assignees",
     *     operationId="emailProcessTask",
     *     tags={"Task"},
     *     produces={
     *         "application/json"
     *     },
     *     @SWG\Parameter(
     *         name="org",
     *         type="string",
     *         description="Organization ID",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="run_id",
     *         type="string",
     *         description="Process ID",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="string",
     *         description="Task ID",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success response",
     *         schema = @SWG\Definition (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Task"
     *              )
     *         )
     *     )
     * )
     */
    public function email($orgId, $runId, $id)
    {
        set_tenant($orgId);
        $run = Run::findOrFail($runId);
        $task = $run->tasks()->findOrFail($id);

        $this->resendTaskToAssignees($task);

        return $this->response->withItem($task, $this->transformer);
    }
}
