<?php namespace App\Http\Controllers\Checklists;

use Dingo\Api\Exception\UpdateResourceFailedException;
use App\Http\Controllers\TenantsController;
use Tallyfy\API\V1\Repositories\Eloquent\RunsRepository;
use Tallyfy\API\V1\Transformers\FileTransformer;
use Tallyfy\API\V1\Transformers\RunTransformer;

class RunsController extends TenantsController
{
    protected $withMap = [
        'checklist' => [
            'checklist.statistics',
            'checklist.kickOffFields'
        ],

        'tasks' => [
            'customtasks.captureValues',
            'customtasks.standaloneTask',
            'customtasks.step',
            'customtasks.guests',
            'customtasks.owners',
            'customtasks.run',
        ],

        'next_task' => [
            'tasks.step'
        ],

        'default' => [
            'captureValues.capture',
            'tasks.threads.participants'
        ]
    ];

    /**
     * @param RunTransformer $transformer
     * @param RunsRepository $repo
     */
    function __construct(RunTransformer $transformer, RunsRepository $repo)
    {
        $this->repository = $repo;
        $this->transformer = $transformer;
        $this->permission_prefix = 'runs';
        $this->defaultSortBy = 'name';
        parent::__construct();
    }

    public function index($org = null)
    {
        if (str_contains(request('status'), 'archived')) {
            $query = $this->repository->query()->withTrashed();
            $this->repository->setQuery($query);
        }

        return parent::index($org);
    }

    public function show($key)
    {
        set_tenant(func_get_arg(0));
        //if the user is a collaborator he should automatically have these permissions
        $this->appendPermissions = \API::user()->appendExtraPermissions(['runs.view' => 1, 'runs.active' => 1]);
        return parent::show($key);
    }

//    public function store()
//    {
//        $request = \App::make('Tallyfy\API\Http\Requests\Runs\CreateRunRequest');
//        $run = $this->repository->create($request);
//        return $this->response->setStatusCode(201)->withItem($run, $this->transformer);
//    }

    public function update($tenant)
    {
        set_tenant($tenant);
        hasAccess('runs.update');
        $id = func_get_arg(1);
        $record = $this->repository->getByKey($id);
        $request = \App::make('Tallyfy\API\Http\Requests\Runs\UpdateRunRequest', ['id' => $id]);
        if ($this->validateInput($record->id)) {
            $run = $this->repository->update($id, $request->all());
            return $this->setStatusCode(200)->withItem($run, $this->transformer);
        }
        throw new UpdateResourceFailedException(\Session::get('error'), $this->validator->errors());

    }

    public function restore($tenant)
    {
        set_tenant($tenant);
        $key = func_get_arg(1);
        hasAccess('runs.update');

        $run = $this->repository->getByKey($key, true, true);
        if ($run->checklist->deleted_at) {
            throw new UpdateResourceFailedException('Please unarchive the template first.');
        }

        $run = $this->repository->restore($key);

        return $this->setStatusCode(200)->withItem($run, $this->transformer);

    }

    /**
     * @SWG\Post(
     *     path="/organizations/{org}/runs/{run_id}/export",
     *     description="Exports Process information into CSV format",
     *     summary="Export a Process",
     *     operationId="exportRun",
     *     produces={"application/json"},
     *     tags={"Process"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization Id",
     *         required=true,
     *         type="string"
     *     ),
     *
     *      @SWG\Parameter(
     *         name="run_id",
     *         in="path",
     *         description="Process ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success response"
     *     )
     * )
     */
    public function export($tenant, $id)
    {
        set_tenant($tenant);
        $file = $this->repository->export($id, $this->request);
        return $this->withItem($file, new FileTransformer());
    }

    /**
     * @SWG\Put(
     *     path="/organizations/{org}/runs/{run_id}/activate",
     *     description="Unarchive a Process",
     *     summary="Unarchive a Process",
     *     operationId="activateRun",
     *     produces={"application/json"},
     *     tags={"Process"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization Id",
     *         required=true,
     *         type="string"
     *     ),
     *
     *      @SWG\Parameter(
     *         name="run_id",
     *         in="path",
     *         description="Process ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success response",
     *         @SWG\Schema(ref="#/definitions/Run")
     *     )
     * )
     */

    public function activate($tenant, $id)
    {
        set_tenant($tenant);

        //the activate functionality is called every time the run active page is loaded (no matter if the run is archived or not) so first check if the run is actually archived
        $run = $this->repository->getByKey($id, true, true);

        // If checklist is archived
//        if ($run->checklist->deleted_at) {
//            throw new UpdateResourceFailedException('Please unarchive the template first.');
//        }

        if ($run->deleted_at) {
            $run = $this->repository->restore($id);
        }

        return $this->withItem($run, $this->transformer);
    }
}
