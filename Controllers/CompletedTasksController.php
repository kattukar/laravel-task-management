<?php
namespace App\Http\Controllers;

use Tallyfy\API\App\Services\OneOffTaskService;
use Tallyfy\API\V1\Transformers\TaskTransformer;

class CompletedTasksController extends BaseControllerNew
{
    protected $service;
    protected $transformer;

    public function __construct(OneOffTaskService $service, TaskTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;

        parent::__construct();
    }

    /**
     * @SWG\Post(
     *     path="/organizations/{org}/completed-tasks",
     *     description="Mark a Task 'completed'",
     *     summary="Mark a Task completed",
     *     operationId="markOneoffCompleted",
     *     produces={"application/json"},
     *     tags={"Task"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization ID",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Definition (
     *              @SWG\Property(
     *                  property="task_id",
     *                  type="string"
     *              )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success response",
     *         schema = @SWG\Definition (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Task"
     *              )
     *         )
     *     )
     * )
     */
    public function store()
    {
        $request = \App::make('Tallyfy\API\Http\Requests\Tasks\MarkTaskCompleteRequest');

        $task = $this->service->markTaskComplete($request->get('task_id'));

        $request->merge(['with' => 'completable_tasks']);
        return $this->response->withItem($task, $this->transformer);
    }

    /**
     * @SWG\DELETE(
     *     path="/organizations/{org}/completed-tasks/{id}",
     *     description="Re-open a completed Task",
     *     summary="Re-open a completed Task",
     *     operationId="markOneoffNotCompleted",
     *     produces={"application/json"},
     *     tags={"Task"},
     *     @SWG\Parameter(
     *         name="org",
     *         in="path",
     *         description="Organization ID",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success response",
     *         schema = @SWG\Definition (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Task"
     *              )
     *         )
     *     )
     * )
     */
    public function destroy($orgID, $taskID)
    {
        \App::make('Tallyfy\API\Http\Requests\Tasks\MarkTaskActiveRequest');

        $task = $this->service->markTaskActive($taskID);

        return $this->response->withItem($task, $this->transformer);
    }
}