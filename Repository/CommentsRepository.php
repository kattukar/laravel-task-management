<?php namespace Tallyfy\API\V1\Repositories\Eloquent;

use Tallyfy\API\V1\Models\Comment;
use Tallyfy\API\V1\Transformers\CommentTransformer;

class CommentsRepository extends BaseRepository
{
    function __construct(Comment $model, CommentTransformer $transformer)
    {
        parent::__construct($model, $transformer);
    }

    function filter($prefixes = [])
    {
        //check if the commentable_id exists in the corresponding table (because some run ids did not)
        $commentIDs = [];
        foreach ($this->query->get() as $comment) {
            if (isset($comment->commentable->id)) {
                $commentIDs[] = $comment->id;
            }
        }

        $this->query()->whereIn('id', $commentIDs)->get();

        return parent::filter();
    }
}