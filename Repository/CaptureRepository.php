<?php namespace Tallyfy\API\V1\Repositories\Eloquent;

use Tallyfy\API\V1\Models\Capture;
use Tallyfy\API\V1\Transformers\CaptureTransformer;

class CaptureRepository extends BaseRepository
{

    public function __construct(Capture $model, CaptureTransformer $transformer)
    {
        parent::__construct($model, $transformer);
    }

    public function sync($step, $data)
    {
        $i = 1;
        $ids = [];
        foreach ($data as $input) {
            $capture = null;
            $id = $input['id'] ?? null;
            if ($id) {
                $capture = $this->model->where('timeline_id', $id)->first();
            }

            if ($capture) {
                $capture->modify(array_except($input, ['field_type','step_id']));
            }
            else {
                $capture = $this->model->make($step->id, $input);
            }

            $capture->position = $i;
            $capture->save();
            $ids[] = $capture->id;
            $i++;
        }
        $step->captures()->whereNotIn('id', $ids)->forceDelete();
    }
}