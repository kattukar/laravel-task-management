<?php namespace Tallyfy\API\V1\Repositories\Eloquent;

use DB;
use Tallyfy\API\V1\Models\Checklist;
use Tallyfy\API\V1\Transformers\ChecklistTransformer;

class ChecklistsRepository extends BaseRepository
{
    public function __construct(Checklist $model, ChecklistTransformer $transformer)
    {
        parent::__construct($model, $transformer);
    }

    public function save($checklist)
    {
        \DB::transaction(function() use ($checklist) {
            parent::save($checklist);

            $checklist->contributors()->save();
            $checklist->preruns()->save();
        });

        return $checklist;
    }

    public function findByID($id, $withArchived = false)
    {
        $query = Checklist::getModel();

        if ($withArchived)
        {
            $query = $query::withTrashed();
        }

//        return $query->where('id', $id)->orWhere('alias', $id)->firstOrFail();
        return $query->where('id', $id)->firstOrFail();
    }

    public function filter($prefixes = [])
    {
        parent::filter($prefixes);

        if (\Input::get('archived') == "only") {
            $this->query = $this
                ->query()
                ->whereNotExists(function ($query) {
                    $query->select([])
                        ->from('checklists as c')
                        ->whereRaw('c.timeline_id = checklists.timeline_id')
                        ->whereRaw('c.source_id = checklists.id')
                        ->whereRaw('c.organization_id = checklists.organization_id'); // is not required, but for optimization
                })
                ->onlyTrashed();
        }

        return $this;
    }

    public function getByKey($key, $fail = true, $includeArchived = false)
    {
        if ($includeArchived) {
            $this->query = $this->query()->withTrashed();
        }

//        $query = $this->query()->where('id', $key)->orWhere('alias', $key);
        $query = $this->query()->where('id', $key);

        if ($fail) {
            return $query->firstOrFail();
        }

        return $query->first();
    }

    public function update($key, $input)
    {
        /** @var Checklist $checklist */
        $checklist = $this->getByKey($key);
        \Event::fire('checklist.updating', $input);
        foreach ($input as $attr => $value) {
            $checklist->{$attr} = $value;
        }
//        $checklist->alias = $checklist->makeAlias();

        $checklist->save();

        \Event::fire('checklist.updated', [$checklist, $input]);

        return $checklist;
    }

    public function cloneChecklist($original, $new_title = null, $user = null)
    {
        $id = DB::selectOne(
            "select (clone(c, false, ?, ?)).id from checklists c where id = ?",
            [$user->id ?? auth_user()->id, get_tenant()->id, $original->id]
        )->id;
        $checklist = Checklist::findOrFail($id);
        if (!is_null($new_title)) {
            $checklist->title = $new_title;
            $checklist->save();
        }

        return $checklist;
    }
}
