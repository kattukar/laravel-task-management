<?php
namespace Tallyfy\API\V1\Repositories\Eloquent;

use Tallyfy\API\V1\Models\Branding;

class BrandingRepository extends BaseRepository
{
    public function __construct(Branding $model)
    {
        parent::__construct($model);
    }

    public function getCustomBranding()
    {
        return $this->model->first();
    }

    public function delete($orgID)
    {
        return $this->model->where('organization_id', $orgID)->delete();
    }

}