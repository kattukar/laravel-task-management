<?php namespace Tallyfy\API\V1\Repositories\Eloquent;

use Tallyfy\API\V1\Models\Asset;
use Tallyfy\API\V1\Transformers\AssetTransformer;

class AssetsRepository extends BaseRepository
{

    function __construct(Asset $model, AssetTransformer $transformer)
    {
        parent::__construct($model, $transformer);
    }

    public function addAsset($fileName, $input)
    {
        //check if an asset with that filename already exists and if it does, only increment the version
        $assetQuery = $this->query()
            ->where('filename', $fileName)
            ->where('uploaded_from', $input['uploaded_from'])
            ->where('subject_type', $input['subject_type'])
            ->where('subject_id', $input['subject_id']);
        if(isset($input['step_id'])) {
            $assetQuery->where('step_id', $input['step_id']);
        }
        if ($asset = $assetQuery->first()) {
            //update file version
            $assetQuery->update(['version' => $asset->version + 1]);
            //get the updated asset with the new version
            $asset = $assetQuery->first();
        } else {
            $fileData = [
                'filename' => $fileName,
                'version' => 1,
                'subject_id' => $input['subject_id'],
                'subject_type' => $input['subject_type'],
                'uploaded_from' => $input['uploaded_from']
            ];
            if(isset($input['step_id'])) {
                $fileData['step_id'] = $input['step_id'];
            }
            $asset = $this->create($fileData);
        }

        return $asset;
    }

    /**
     * Get Asset if it was labeled as logo when uploaded
     */
    public function getLogoByKey($key)
    {
        try {
            $asset = $this->query()
                    ->where('id', $key)
                    ->where('uploaded_from', 'logo')
                    ->withTrashed()
                    ->firstOrFail();
        }
        catch (\Exception $e) {
            throw new \Tallyfy\API\Domain\Exceptions\AssetNotFoundSilentException(null, 404, $e);
        }

        return [
            'subject_type' => str_replace('\\', '', substr($asset->subject_type, strrpos($asset->subject_type, '\\'))),
            'subject_id' => $asset->subject_id,
            'asset_name_id' => $asset->id .'.'. strtolower(pathinfo($asset->filename, PATHINFO_EXTENSION)),
            'asset_name' => $asset->filename
        ];
    }

}