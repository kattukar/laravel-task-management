<?php namespace Tallyfy\API\V1\Repositories\Eloquent;

use Tallyfy\API\V1\Models\Country;
use Tallyfy\API\V1\Transformers\CountriesTransformer;

class CountriesRepository extends BaseRepository
{
    function __construct(Country $model, CountriesTransformer $transformer)
    {
        parent::__construct($model, $transformer);
    }

}